# Common Lisp Practice
Some cleanup to this repository, as I want to organize both how and what I'm
learning.

### Directories
The directory structure will be simple as it'll follow what I'm working on.
Within each directory there will be all the `.lisp` files and a README in each
on giving more context as to what is within (in case you or myself forget?). 

- basic/ - All the work from the 'Practical Common Lisp Programming' book
- challenges/ - From the '/g/ Programming Challenges list v1.4e'
- landoflisp/ - Examples and works from the 'Land of Lisp' book
- org/ - Any org notes taken to expand upon what I've learned

This will change as I continue to learn more in regards to lisp, this structure
at least allows me to know what I'm working on easier and keep it clean all
within one project directory/repository.
